package com.test;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OvAppTest
{
    @Test
    public void testCase()
    {
        OvApp ovApp = new OvApp();
        assertEquals(1600,ovApp.stringTimeToInt("16:00"));
    }

}