package com.test;

import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.*;
import java.util.ArrayList;

public class StationInfo {
    String info;
    File file = new File("StationInfo.json");
    ArrayList<StationInfo> infoList = new ArrayList<StationInfo>();

    StationInfo() {
        getAllStationInfoFromJSON();
    }

    StationInfo(String info) {
        this.info = info;
    }

    public String getInfo() {
        return info;
    }

    public ArrayList<StationInfo> getAllStationInfoFromJSON() {
        try {
            InputStream inputStream = new FileInputStream(file);
            JSONTokener tokener = new JSONTokener(inputStream);
            while (tokener.more()) {
                JSONObject object = new JSONObject(tokener);
                info = object.getString("StationInfo");
                StationInfo newInfo = new StationInfo(info);
                infoList.add(newInfo);
                tokener.next();
            }
        } catch (FileNotFoundException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
        }
        return infoList;
    }

    public void save(ArrayList<StationInfo> arrayList) {
        try {
            FileWriter fileWriter = new FileWriter("StationInfo.json");

            for (StationInfo info : arrayList) {
                if (arrayList.indexOf(info) == arrayList.size() - 1) {
                    fileWriter.write(info.toJson().toString() + ",");
                } else {
                    fileWriter.write(info.toJson().toString() + "," + "\n");
                }
            }

            fileWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public org.json.simple.JSONObject toJson() {
        org.json.simple.JSONObject jsonObject = new org.json.simple.JSONObject();
        jsonObject.put("StationInfo", info);
        return jsonObject;
    }
}
