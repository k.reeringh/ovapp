package com.test;

public class TrainTrip extends Trip implements  Vehicle
{
    TrainTrip(String name, String d_time, String a_time)
    {
        super(name, d_time, a_time);
    }
    @Override
    public String getVehicle()
    {
        return "Train";
    }
}
