package com.test;

import org.json.simple.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class testRouteClass
{
    String name;
    String depStationName;
    String arStationName;
    String d_time;
    String a_time;
    ArrayList<testRouteClass> trainTrips = new ArrayList<testRouteClass>();


    testRouteClass(String name, Station departureStation, Station arrivalStation, String d_time, String a_time)
    {
        this.name = name;
        depStationName = departureStation.statName;
        arStationName = arrivalStation.statName;
        this.d_time = d_time;
        this.a_time = a_time;
    }

    public void addToArrayList(testRouteClass tet)
    {
        trainTrips.add(tet);
    }
    public String getDepStationName()
    {

        return depStationName;
    }

    public String getArStationName()
    {

        return arStationName;
    }

    public String getName()
    {

        return name;
    }

    public String getD_time()
    {

        return d_time;
    }

    public String getA_time()
    {

        return a_time;
    }

    public JSONObject toJson()
    {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("RouteName",name);
        jsonObject.put("DepartureStation",depStationName);
        jsonObject.put("ArrivalStation",arStationName);
        jsonObject.put("DepartureTime",d_time);
        jsonObject.put("ArrivalTime",a_time);

        return jsonObject;
    }
    public ArrayList<testRouteClass> returnArrayList()
    {
        return trainTrips;
    }
}
