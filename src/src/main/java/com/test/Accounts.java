package com.test;

import org.json.JSONTokener;
import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.util.ArrayList;

public class Accounts {
    ArrayList<Account> accounts = new ArrayList<>();

    public void add(Account account)
    {
        accounts.add(account);
    }//TEST push
    public void save( File file )
    {
        try
        {
            FileWriter fileWriter = new FileWriter( file );

            for (Account account : accounts)
            {

                if(accounts.indexOf(account)==accounts.size()-1)
                {
                    fileWriter.write(account.toJSON().toString() + "," );
                }
                else
                {
                    fileWriter.write(account.toJSON().toString() + "," + "\n");
                }
           }

            fileWriter.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public void load(File file) {
        accounts.clear();

        try {
            InputStream inputStream = new FileInputStream(file);
            JSONTokener tokener = new JSONTokener(inputStream);
            while(tokener.more())
            {

                    JSONObject object = new JSONObject(tokener);
                    Account account = new Account(object);

                    accounts.add(account);
                    tokener.next();
            }


        } catch (FileNotFoundException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
        }
    }
    public ArrayList<Account> getAccounts()
    {
        return accounts;
    }

}


