package com.test;

public class BusTrip extends Trip implements  Vehicle
{
    BusTrip(String name, String d_time, String a_time)
    {
        super(name, d_time, a_time);
    }

    @Override
    public String getVehicle()
    {
        return "Bus";
    }
}
