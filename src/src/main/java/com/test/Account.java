package com.test;

import org.json.JSONObject;

import java.util.ArrayList;

public class Account
{
    private String userName;
    private String password;
    private boolean loggedIn;
    private ArrayList<Object> savedRoutes = new ArrayList<>();
    private ArrayList<Object> historyList = new ArrayList<>();


    public Account()
    {

    }
    public Account(JSONObject obj)
    {
        userName = obj.getString("Name");
        password=obj.getString("Password");
        loggedIn = obj.getBoolean("LoggedIn");
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }
    public void setLoggedIn(boolean loggedIn)
    {
        this.loggedIn=loggedIn;
    }
    public boolean getLoggedIn()
    {
        return loggedIn;
    }

    public JSONObject toJSON()
    {
        JSONObject obj = new JSONObject();


        obj.put( "Name", userName );

        obj.put( "Password", password );

        obj.put("LoggedIn",loggedIn);


        return obj;
    }

    public void addSavedRoute(Object object) { this.savedRoutes.add(object);}

    public void addToHistory(Object object) { this.historyList.add(object);}

    public ArrayList<Object> getSavedRoutes() { return this.savedRoutes; }

    public ArrayList<Object> getTravelHistory() { return this.historyList; }


}
