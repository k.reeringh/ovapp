package com.test;

import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.*;
import java.util.ArrayList;

public class Station {
    String name;
    String stationInfo;


    String statName;
    ArrayList<Station> noDuplicateList = new ArrayList<>();


    Station(String stationName, StationInfo stationInfo) {
        name = stationName;
        this.stationInfo = stationInfo.getInfo();
    }


    public static ArrayList<Station> getAllStationsFromJSON() {
        StationInfo station = new StationInfo();
        ArrayList<StationInfo> infoList = station.getAllStationInfoFromJSON();
        File file = new File("Stations.json");
        ArrayList<Station> allStationsList = new ArrayList<>();
        try {
            InputStream inputStream = new FileInputStream(file);
            JSONTokener tokener = new JSONTokener(inputStream);

            while (tokener.more()) {
                JSONObject object = new JSONObject(tokener);
                String stationInfo = object.getString("StationInfo");
                StationInfo info1 = new StationInfo(stationInfo);
                infoList.add(info1);

                String stationName = object.getString("StationName");
                Station stat = new Station(stationName, info1);
                if(!allStationsList.contains(stat)){
                    allStationsList.add(stat);
                }

                tokener.next();
            }
        } catch (FileNotFoundException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
        }
        return allStationsList;
    }

    public String getStationInfo() {
        return stationInfo;
    }

    public String getName() {
        return name;
    }

    public void save(ArrayList<Station> arrayList, ArrayList<StationInfo> infList) {
        try {
            FileWriter fileWriter = new FileWriter("TripJson1.json");

            for (Station trip : arrayList) {
                if (arrayList.indexOf(trip) == arrayList.size() - 1) {
                    fileWriter.write(trip.toJson().toString() + ",");
                } else {
                    fileWriter.write(trip.toJson().toString() + "," + "\n");
                }
            }

            fileWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public org.json.simple.JSONObject toJson() {
        org.json.simple.JSONObject jsonObject = new org.json.simple.JSONObject();
        jsonObject.put("StationName", name);
        jsonObject.put("StationInfo", stationInfo);

        return jsonObject;
    }
/*
    public ArrayList<Station> addwithoutdup(ArrayList<Station> trainTrips1, ArrayList<Station> allStationsList) {
        boolean found = false;
        for (Station t : trainTrips1) {
            found = false;
            for (Station t2 : allStationsList) {
                if (t.getName().equals(t2.getName())) {
                    found = true;
                }
            }
            if (found == false) {
                noDuplicateList.add(t);
            }
        }
        return  noDuplicateList;
    }*/
}




