package com.test;
import org.json.JSONObject;
import org.json.JSONTokener;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

// OvApp is een JFrame, dus het volledige window
public class OvApp extends JFrame {

    ////////////////////////////////////////////////////////////

    JScrollPane jScrollPane;
    DefaultComboBoxModel defaultComboBoxModel1;
    DefaultComboBoxModel defaultComboBoxModel2;
    DefaultListModel defaultListModel;
    //JButton btnSearchForTrips;
    JButton btnHistoryList = new JButton("History");
    private JButton btnSavedRoutes = new JButton("Favorites");
    private JButton btnSaveRoute = new JButton("Save trip");;
    JList jList;
    JList selectedTrips;
    JComboBox availableRoutes;
    JComboBox availableTimes;

    ArrayList<Trip> travelHistory;
    ArrayList<Trip> savedRoutes;
    ArrayList<Account> accounts;
    ArrayList<Object> historyList;
    Account loggedInAccount;

    //    Route[] r = {r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12};
    String[] times = {"08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00", "22:00", "23:00"};
    ArrayList<TrainTrip> trips = new ArrayList<>();
    ArrayList<BusTrip> busTrips = new ArrayList<>();
    ArrayList<TrainTrip> trainTrips = new ArrayList<>();

    ////////////////////////////////////////////////////////////

    // *
    private Account lastLoggedInAccount = new Account();//the last logged in account .
    private Accounts accounts1 = new Accounts();//array list of the accounts .
    private final String fname = "accountsJson.json";//the name of the file where the accounts are saved
    private File file = new File(fname);

    // ActionListeners
    OvApp.menuSwitcher menuSwitcher = new menuSwitcher();
//    OvApp.searchRoute searchRoute = new searchRoute();

    // LayoutManager settings
    private CardLayout cardLayout = new CardLayout();
    private GridBagLayout gridBagLayout = new GridBagLayout();
    private GridBagConstraints c = new GridBagConstraints();
    private BorderLayout borderLayout = new BorderLayout();

    List<JButton> buttonList = new ArrayList<JButton>();

    // Panel waarop alle andere panels komen te staan
    private JPanel pnlHolder = new JPanel();

    // Panels voor verschillende menu's
    private JPanel pnlLogin = new JPanel();
    private JPanel pnlHome = new JPanel();
    private JPanel pnlSearchFunction = new JPanel();
    private JPanel pnlRegister = new JPanel();
    private JPanel pnlEdit = new JPanel();
    private int languageValue = 0;

    // Onderdelen van panelLogin (loginscherm)
    private JButton btnLogin = new JButton("Login");
    private JButton btnReset = new JButton("Reset");
    private JButton btnToRegister = new JButton("Sign up");
    private JButton btnContinueWithoutAccount = new JButton("Continue");
    private JTextField txtUsername = new JTextField();
    private JPasswordField txtPassword = new JPasswordField();
    private JLabel lblUsername = new JLabel("Username:");
    private JLabel lblPassword = new JLabel("Password:");
    private JPanel pnlHolderLogin = new JPanel();
    private JPanel pnlErrorMessageLogin = new JPanel();
    private JLabel lblErrorMessageLogin = new JLabel();
    private TitledBorder a = BorderFactory.createTitledBorder("Login");
    private JRadioButton rememberMeBtn = new JRadioButton("Remember me");

    // Onderdelen van registratieScherm
    private JLabel lblUsernameRegister = new JLabel("Username:");
    private JLabel lblPasswordRegister = new JLabel("Password:");
    private JTextField txtUsernameRegister = new JTextField();
    private JPasswordField txtPasswordRegister = new JPasswordField();
    private JButton btnSignUp = new JButton("Sign up");
    private JButton btnResetRegister = new JButton("Reset");
    private JButton btnGoBack = new JButton("Go Back");
    private JPanel pnlHolderRegister = new JPanel();
    private JPanel pnlErrorMessageRegister = new JPanel();
    private JLabel lblErrorMessageRegister = new JLabel();
    private TitledBorder b = BorderFactory.createTitledBorder("Register");

    // Onderdelen van panelHome (Homescherm)
    private JButton btnLogout = new JButton("Logout");
    private JButton btnToSearchFunctionFromHome = new JButton("Plan");
    private JButton btnToNewFunction = new JButton("Coming soon");
    private JPanel pnlWest = new JPanel();
    private JButton btnCloseProgram = new JButton("Exit");
    private Border raisedetched = BorderFactory.createEtchedBorder(EtchedBorder.RAISED);//.

    // Onderdelen van Edit panel
    private JPanel panelEditHolder = new JPanel();
    private JPanel panelEditPassword = new JPanel();
    private TitledBorder userNameBorder = new TitledBorder("Username");
    private TitledBorder passwordBorder = new TitledBorder("Password");
    private JButton doneBtn = new JButton("Done");
    private JButton editUserName = new JButton("Edit username");
    private JButton editPassWord = new JButton("Edit password");
    private JTextField oldUserName = new JTextField();
    private JTextField newUserName = new JTextField();
    private JTextField oldPassWord = new JTextField();
    private JTextField newPassWord = new JTextField();
    private JTextField newPassWord1 = new JTextField();
    private JLabel newUsername = new JLabel("New username");
    private JLabel oldUsername = new JLabel("Old username");
    private JLabel newPassword = new JLabel("New password");
    private JLabel oldPassword = new JLabel("Old password");
    private JLabel passwordError = new JLabel("Your old password is not correct");
    private JLabel passwordEdited = new JLabel("Your password has been changed");
    private JLabel userNameError = new JLabel("Your old username is not correct");
    private JLabel userNameEdited = new JLabel("Your username has been changed");
    private JLabel emptyError = new JLabel("fill out all fields ");
    private JLabel emptyError1 = new JLabel("fill out all fields ");
    private JLabel takenAccountError = new JLabel("This username is already used");


    // Onderdelen van panelSearchScreen
    private JPanel pnlL;
    private JPanel pnlR;
    private JPanel pnlRoute = new JPanel();
    private JPanel pnlStationInfo = new JPanel();
    private JPanel pnlMap = new JPanel();
    private JButton btnSearchForTrips = new JButton("Search");
    private JButton btnTerugNaarHomeVanuitSearchFunction = new JButton();
    private JLabel image = new JLabel();
    private JLabel departure_Label = new JLabel();
    private JLabel arrival_Label = new JLabel();
    private JLabel departureTime_Label = new JLabel();
    private JLabel arrivalTime_Label = new JLabel();
    private JLabel tripInfo_Label = new JLabel("Your travel information: ");
    private JLabel dInfo = new JLabel();
    private JLabel aInfo = new JLabel();

    private JComboBox departureTimes = new JComboBox();
    //    private String[] showStation = route.showList().toArray(new String[0]);
    private String[] arrayTimes = {"08:00", "08:30", "09:00", "09:30", "10:00", "10:30", "11:00", "11:30", "12:00"};
    private JComboBox cbbRoutes;
    private JComboBox cbbTimes;
    private DefaultListModel listModel;
    private JButton btnMap = new JButton("Show map");
    private JFrame map_window = new JFrame();
    private JList sortedTripDisplay;
    private JScrollPane sortedTripScroller;

    private JRadioButton rbtnBus = new JRadioButton("Bus");
    private JRadioButton rbtnTrain = new JRadioButton("Train");

    // Menubar
    private JMenuBar menuBar;
    private JMenu menuLeave;
    private JMenu menuTranslate;
    private JMenuItem jMenuItemClose;
    private JMenuItem jMenuItemLogout;
    private JMenuItem jMenuIteamDutch;
    private JMenuItem jMenuItemEnglish;
    private JMenu  menuMyProfile = new JMenu("My Profile");
    private JMenuItem jMenuEditProfile = new JMenuItem("Edit Profile");
    private JMenuItem jMenuItemDeleteProfile = new JMenuItem("Delete Profile");

    OvApp() {
        File file1 = new File("TripsJson.json");
        File file2 = new File("BusTripsJSON.json");
        loadRoutesJSONTrain(file1, trips);
        loadRoutesJSONBus(file2, busTrips);
        System.out.println("Test" + trips.size() + trips.get(1).getName());
        System.out.println("TEST " + busTrips.size() + " " + busTrips.get(1).getName());
        accounts1.load(file);


        setMenuBar();

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(700, 720);
        this.setTitle("OvApp");
        this.setLayout(null);
        this.setResizable(true);
        this.setContentPane(pnlHolder);
        this.setJMenuBar(menuBar);
        this.setVisible(true);

        pnlHolder.setLayout(cardLayout);
        pnlHolder.add(pnlLogin, "panelLogin");
        pnlHolder.add(pnlHome, "panelHome");
        pnlHolder.add(pnlSearchFunction, "panelSearchScreen");
        pnlHolder.add(pnlRegister, "registratieScherm");
        pnlHolder.add(pnlEdit,"EditProfilePanel");

        revalidate();
    }

    public void setMenuBar() {
        menuBar = new JMenuBar();

        menuLeave = new JMenu("Leave");
        menuTranslate = new JMenu("Translate");

        jMenuItemClose = new JMenuItem("Close program");
        jMenuItemLogout = new JMenuItem("Logout");

        jMenuItemEnglish = new JMenuItem("English");
        jMenuIteamDutch = new JMenuItem("Nederlands");





        jMenuIteamDutch.addActionListener((e) ->
        {

            btnLogin.setText("Inloggen");
            btnReset.setText("Opnieuw");
            btnToRegister.setText("Aanmelden");
            btnContinueWithoutAccount.setText("Verder");
            lblUsername.setText("Gebruikersnaam:");
            lblPassword.setText("Wachtwoord:");

            lblUsernameRegister.setText("Gebruikersnaam:");
            lblPasswordRegister.setText("Wachtwoord:");
            btnSignUp.setText("Aanmelden");
            btnResetRegister.setText("Opnieuw");
            btnGoBack.setText("Terug");

            btnLogout.setText("Uitloggen");
            btnToSearchFunctionFromHome.setText("Zoeken");
            btnToNewFunction.setText("Binnenkort ");
            btnCloseProgram.setText("Afsluiten");

            btnSearchForTrips.setText("Zoeken");
            btnTerugNaarHomeVanuitSearchFunction.setText("Thuisscherm ");

            rbtnBus.setText("Bus");
            rbtnTrain.setText("Train");

            a.setTitle("Aanmelden");
            b.setTitle("Registreren");
            tripInfo_Label.setText("Uw reisinformatie: ");
            btnSavedRoutes.setText("Favorieten");
            btnHistoryList.setText("Historie");
            btnSaveRoute.setText("Reis Opslaan");

            languageValue = 1;
        });

        jMenuItemEnglish.addActionListener(e -> {

            btnLogin.setText("Login");
            btnReset.setText("Reset");
            btnToRegister.setText("Sign up");
            btnContinueWithoutAccount.setText("Continue");
            lblUsername.setText("Username:");
            lblPassword.setText("Password:");

            lblUsernameRegister.setText("Username:");
            lblPasswordRegister.setText("Password:");
            btnSignUp.setText("Sign up");
            btnResetRegister.setText("Reset");
            btnGoBack.setText("Go Back");

            btnLogout.setText("Logout");
            btnToSearchFunctionFromHome.setText("Search");
            btnToNewFunction.setText("Coming soon");
            btnCloseProgram.setText("Exit");

            btnSearchForTrips.setText("Search");
            btnTerugNaarHomeVanuitSearchFunction.setText("Home");

            rbtnBus.setText("Bus");
            rbtnTrain.setText("Trein");

            a.setTitle("Login");
            b.setTitle("Register");

            tripInfo_Label.setText("Your travel information: ");
            btnSavedRoutes.setText("Favorites");
            btnHistoryList.setText("History");
            btnSaveRoute.setText("Save trip");
            languageValue = 0;
        });


        jMenuItemClose.addActionListener((e) ->
        {
            accounts1.save(file);
            System.exit(0);
        });

        jMenuItemLogout.addActionListener((e) -> {
            loggedInAccount=null;
            setFalse();
            setPanelLogin();
            cardLayout.show(pnlHolder, "panelLogin");
        });

        jMenuItemDeleteProfile.addActionListener((e) ->
        {
            if(loggedInAccount==null)
            {
            }
            else{
            String userName = loggedInAccount.getUserName();
            for(Account a : accounts1.getAccounts())
            {
                if(a.getUserName().equals(userName))
                {
                    accounts1.getAccounts().remove(a);
                    break;
                }
            }
            setPanelLogin();
            cardLayout.show(pnlHolder, "panelLogin");
        }});

        jMenuEditProfile.addActionListener(e ->
        {
            if(loggedInAccount==null){}
            else
            {
                setPanelEditProfile();
                cardLayout.show(pnlHolder,"EditProfilePanel");
            }
        });

        menuLeave.add(jMenuItemLogout);
        menuLeave.add(jMenuItemClose);

        menuTranslate.add(jMenuItemEnglish);
        menuTranslate.add(jMenuIteamDutch);

        menuMyProfile.add(jMenuEditProfile);
        menuMyProfile.add(jMenuItemDeleteProfile);

        menuBar.add(menuLeave);
        menuBar.add(menuTranslate);
        menuBar.add(menuMyProfile);
    }

    public void setPanelLogin() {
        pnlLogin.setBackground(new Color(245, 250, 255));
        pnlLogin.setLayout(gridBagLayout);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = new Insets(3, 3, 3, 3);

        c.gridx = 0;
        c.gridy = 0;
        pnlHolderLogin.setBackground(new Color(245, 255, 250));
        pnlHolderLogin.setPreferredSize(new Dimension(300, 200));
        a.setTitleJustification(TitledBorder.CENTER);
        pnlHolderLogin.setBorder(a);
        pnlHolderLogin.setLayout(gridBagLayout);
        pnlLogin.add(pnlHolderLogin, c);

        c.gridwidth = 1;
        c.gridheight = 1;
        c.gridx = 0;
        c.gridy = 1;
        pnlErrorMessageLogin.setVisible(false);
        pnlErrorMessageLogin.setBackground(new Color(245, 255, 250));
        pnlErrorMessageLogin.setPreferredSize(new Dimension(250, 30));
        pnlErrorMessageLogin.setLayout(gridBagLayout);
        pnlLogin.add(pnlErrorMessageLogin, c);

        lblErrorMessageLogin.setText("Gebruikersnaam is nog niet geregistreerd");
        lblErrorMessageLogin.setFont(new Font("Comic sans", Font.ITALIC, 12));
        lblErrorMessageLogin.setForeground(Color.BLACK);
        pnlErrorMessageLogin.add(lblErrorMessageLogin, c);

        c.gridwidth = 1;
        c.gridheight = 1;
        c.gridx = 0;
        c.gridy = 0;
        pnlHolderLogin.add(lblUsername, c);

        c.gridwidth = 1;
        c.gridheight = 1;
        c.gridx = 0;
        c.gridy = 1;
        pnlHolderLogin.add(lblPassword, c);

        c.gridwidth = 2;
        c.gridheight = 1;
        c.gridx = 1;
        c.gridy = 0;
        pnlHolderLogin.add(txtUsername, c);

        c.gridwidth = 2;
        c.gridheight = 1;
        c.gridx = 1;
        c.gridy = 1;
        pnlHolderLogin.add(txtPassword, c);

        c.gridwidth = 2;
        c.gridheight = 1;
        c.gridx = 1;
        c.gridy = 2;
        rememberMeBtn.setBackground(new Color(245, 255, 250));
        rememberMeBtn.addActionListener(e -> {});
        pnlHolderLogin.add(rememberMeBtn , c);

        c.gridwidth = 2;
        c.gridheight = 1;
        c.gridx = 1;
        c.gridy = 3;
        btnReset.setFocusable(false);
        btnReset.addActionListener((e) ->
        {
            if (e.getSource() == btnReset) {
                txtUsername.setText("");
                txtPassword.setText("");
            }
            if (e.getSource() == btnResetRegister) {
                txtUsernameRegister.setText("");
                txtPasswordRegister.setText("");
            }
        });
        pnlHolderLogin.add(btnReset, c);

        c.gridwidth = 2;
        c.gridheight = 1;
        c.gridx = 1;
        c.gridy = 4;
        btnLogin.setFocusable(false);
        btnLogin.addActionListener((e) ->
        {
            for (Account a : accounts1.getAccounts()) {
                if (txtUsername.getText().equals(a.getUserName()) && String.valueOf(txtPassword.getPassword()).equals(a.getPassword())) {
                    setPanelHome();
                    cardLayout.show(pnlHolder, "panelHome");
                    loggedInAccount = a;
                    if(rememberMeBtn.isSelected())
                    {
                        setTrue(loggedInAccount);
                    }
                    else
                    {
                        setFalse();
                    }
                } else if (txtUsername.getText().equals("") || String.valueOf(txtPassword.getPassword()).equals("")) {
                    lblErrorMessageLogin.setText("Vul alle velden in");
                    pnlErrorMessageLogin.setVisible(true);
                } else {
                    lblErrorMessageLogin.setText("Account bestaat niet");
                    pnlErrorMessageLogin.setVisible(true);
                }
            }

            txtUsername.setText("");
            txtPassword.setText("");
        });

        pnlHolderLogin.add(btnLogin, c);

        c.gridwidth = 1;
        c.gridheight = 1;
        c.gridx = 1;
        c.gridy = 5;
        btnToRegister.setFocusable(false);
        btnToRegister.addActionListener(menuSwitcher);
        pnlHolderLogin.add(btnToRegister, c);

        c.gridwidth = 1;
        c.gridheight = 1;
        c.gridx = 2;
        c.gridy = 5;
        btnContinueWithoutAccount.setFocusable(false);
        btnContinueWithoutAccount.addActionListener(e ->{

                setPanelSearchFunction();
                cardLayout.show(pnlHolder, "panelSearchScreen");
                btnTerugNaarHomeVanuitSearchFunction.setVisible(false);

        });
        pnlHolderLogin.add(btnContinueWithoutAccount, c);

       // setPanelRegister();
        revalidate();
    }


    public void setPanelEditProfile()
        {
            pnlEdit.setBackground(new Color(245,250,255));
            pnlEdit.setPreferredSize(new Dimension(300,200));
            pnlEdit.setLayout(gridBagLayout);

            c.gridwidth = 1;
            c.gridheight = 1;
            c.gridx = 0;
            c.gridy = 0;
            panelEditHolder.setBackground(new Color(200,250,255));
            panelEditHolder.setPreferredSize(new Dimension(300,200));
            userNameBorder.setTitleJustification(TitledBorder.CENTER);
            panelEditHolder.setBorder(userNameBorder);
            panelEditHolder.setLayout(gridBagLayout);
            pnlEdit.add(panelEditHolder,c);

            c.gridwidth = 1;
            c.gridheight = 1;
            c.gridx = 1;
            c.gridy = 0;
            panelEditPassword.setBackground(new Color(200,250,255));
            panelEditPassword.setPreferredSize(new Dimension(300,200));
            passwordBorder.setTitleJustification(TitledBorder.CENTER);
            panelEditPassword.setBorder(passwordBorder);
            panelEditPassword.setLayout(gridBagLayout);
            pnlEdit.add(panelEditPassword,c);


            c.gridwidth = 1;
            c.gridheight = 1;
            c.gridx = 0;
            c.gridy = 0;
            panelEditHolder.add(oldUsername,c);

            c.gridwidth = 1;
            c.gridheight = 1;
            c.gridx = 1;
            c.gridy = 0;
            oldUserName.setPreferredSize(new Dimension(150,25));
            panelEditHolder.add(oldUserName,c);

            c.gridwidth = 1;
            c.gridheight = 1;
            c.gridx = 0;
            c.gridy = 1;
            panelEditHolder.add(newUsername,c);

            c.gridwidth = 1;
            c.gridheight = 1;
            c.gridx = 1;
            c.gridy = 1;
            newUserName.setPreferredSize(new Dimension(150,25));
            panelEditHolder.add(newUserName,c);

            c.gridwidth = 1;
            c.gridheight = 1;
            c.gridx = 1;
            c.gridy = 2;
            editUserName.addActionListener(e ->
            {
                userNameError.setVisible(false);
                userNameEdited.setVisible(false);
                emptyError1.setVisible(false);
                takenAccountError.setVisible(false);
                String s = oldUserName.getText();
                String s1 = newUserName.getText();
                String s2 = loggedInAccount.getUserName();

                if(s.equals("")||s1.equals(""))
                {
                    emptyError1.setVisible(true);
                }
                else{
                if( s.equals(s2)&& !checkAccount1(s1))
                {
                    for(Account account : accounts1.getAccounts())
                    {
                        if(account.getUserName().equals(s2))
                        {
                            account.setUserName(s1);
                            break;
                        }
                    }
                    userNameEdited.setVisible(true);
                }
                else if (s.equals(s2) && checkAccount1(s1))
                {
                    takenAccountError.setVisible(true);
                }
                else if (!s.equals(s2))
                {
                    userNameError.setVisible(true);
                }
            }});
            editUserName.setPreferredSize(new Dimension(120,20));

            panelEditHolder.add(editUserName,c);

            c.gridwidth = 1;
            c.gridheight = 1;
            c.gridx =1;
            c.gridy = 3;
            userNameError.setVisible(false);
            panelEditHolder.add(userNameError,c);


            c.gridwidth = 1;
            c.gridheight = 1;
            c.gridx =1;
            c.gridy = 3;
            userNameEdited.setVisible(false);
            panelEditHolder.add(userNameEdited,c);

            c.gridwidth = 1;
            c.gridheight = 1;
            c.gridx =1;
            c.gridy = 3;
            emptyError1.setVisible(false);
            panelEditHolder.add(emptyError1,c);

            c.gridwidth = 1;
            c.gridheight = 1;
            c.gridx =1;
            c.gridy = 3;
            takenAccountError.setVisible(false);
            panelEditHolder.add(takenAccountError,c);


            c.gridwidth = 1;
            c.gridheight = 1;
            c.gridx = 0;
            c.gridy = 0;
            panelEditPassword.add(oldPassword,c);

            c.gridwidth = 1;
            c.gridheight = 1;
            c.gridx = 1;
            c.gridy = 0;
            oldPassWord.setPreferredSize(new Dimension(150,25));
            panelEditPassword.add(oldPassWord,c);

            c.gridwidth = 1;
            c.gridheight = 1;
            c.gridx = 0;
            c.gridy = 1;
            panelEditPassword.add(newPassword,c);

            c.gridwidth = 1;
            c.gridheight = 1;
            c.gridx = 1;
            c.gridy = 1;
            newPassWord.setPreferredSize(new Dimension(150,25));
            panelEditPassword.add(newPassWord,c);


            c.gridwidth = 1;
            c.gridheight = 1;
            c.gridx = 1;
            c.gridy = 3;
            editPassWord.setPreferredSize(new Dimension(120,20));
            editPassWord.addActionListener(e ->
            {
                System.out.println("Test");
                passwordError.setVisible(false);
                passwordEdited.setVisible(false);
                emptyError.setVisible(false);
                String s = oldPassWord.getText();
                System.out.println(s);
                String s1 = newPassWord.getText();
                System.out.println(s1);
                String s2 = loggedInAccount.getPassword();
                System.out.println(s2);
                if(s.equals("") || s1.equals(""))
                {
                    emptyError.setVisible(true);
                }
                else{
                if(s.equals(s2))
                {
                    System.out.println("If");
                    for(Account account : accounts1.getAccounts())
                    {
                        System.out.println("for");
                        if(account.getPassword().equals(s2))
                        {
                            System.out.println("If2");
                            account.setPassword(s1);
                            passwordEdited.setVisible(true);
                            break;
                        }


                    }
                }
                if(!s.equals(s2))
                {
                    System.out.println("doesnt match");
                    passwordError.setVisible(true);
                }}
            });
            panelEditPassword.add(editPassWord,c);

            c.gridwidth = 1;
            c.gridheight = 1;
            c.gridx = 1;
            c.gridy = 4;
            passwordError.setVisible(false);
            panelEditPassword.add(passwordError,c);

            c.gridwidth = 1;
            c.gridheight = 1;
            c.gridx = 1;
            c.gridy = 4;
            passwordEdited.setVisible(false);
            panelEditPassword.add(passwordEdited,c);

            c.gridwidth = 1;
            c.gridheight = 1;
            c.gridx = 1;
            c.gridy = 4;
            emptyError.setVisible(false);
            panelEditPassword.add(emptyError,c);


            c.gridwidth = 1;
            c.gridheight = 1;
            c.gridx = 1;
            c.gridy = 3;
            doneBtn.addActionListener(e ->
            {
                setPanelLogin();
                cardLayout.show(pnlHolder,"panelHome");
            });
            pnlEdit.add(doneBtn,c);

        }

    public void setPanelRegister() {
        pnlRegister.setBackground(new Color(245, 250, 255));
        pnlRegister.setLayout(gridBagLayout);

        c.gridwidth = 1;
        c.gridheight = 1;
        c.gridx = 0;
        c.gridy = 0;
        pnlHolderRegister.setBackground(new Color(245, 255, 250));
        pnlHolderRegister.setPreferredSize(new Dimension(300, 200));
        b.setTitleJustification(TitledBorder.CENTER);
        pnlHolderRegister.setBorder(b);
        pnlHolderRegister.setLayout(gridBagLayout);
        pnlRegister.add(pnlHolderRegister, c);

        c.gridwidth = 1;
        c.gridheight = 1;
        c.gridx = 0;
        c.gridy = 1;
        pnlErrorMessageRegister.setVisible(false);
        pnlErrorMessageRegister.setBackground(new Color(245, 255, 250));
        pnlErrorMessageRegister.setPreferredSize(new Dimension(250, 30));
        pnlErrorMessageRegister.setLayout(gridBagLayout);
        pnlRegister.add(pnlErrorMessageRegister, c);

        c.gridwidth = 1;
        c.gridheight = 1;
        c.gridx = 0;
        c.gridy = 0;
        lblErrorMessageRegister.setText("Gebruikersnaam is al geregistreerd");
        lblErrorMessageRegister.setFont(new Font("Comic sans", Font.ITALIC, 12));
        lblErrorMessageRegister.setForeground(Color.BLACK);
        pnlErrorMessageRegister.add(lblErrorMessageRegister, c);

        c.gridwidth = 1;
        c.gridheight = 1;
        c.gridx = 0;
        c.gridy = 0;
        pnlHolderRegister.add(lblUsernameRegister, c);

        c.gridwidth = 1;
        c.gridheight = 1;
        c.gridx = 0;
        c.gridy = 1;
        pnlHolderRegister.add(lblPasswordRegister, c);

        c.gridwidth = 2;
        c.gridheight = 1;
        c.gridx = 1;
        c.gridy = 0;
        pnlHolderRegister.add(txtUsernameRegister, c);

        c.gridwidth = 2;
        c.gridheight = 1;
        c.gridx = 1;
        c.gridy = 1;
        pnlHolderRegister.add(txtPasswordRegister, c);

        c.gridwidth = 2;
        c.gridheight = 1;
        c.gridx = 1;
        c.gridy = 2;
        btnResetRegister.setFocusable(false);
        btnResetRegister.addActionListener((e) ->
        {
            if (e.getSource() == btnResetRegister) {
                txtUsernameRegister.setText("");
                txtPasswordRegister.setText("");
            }
        });
        pnlHolderRegister.add(btnResetRegister, c);

        c.gridwidth = 1;
        c.gridheight = 1;
        c.gridx = 1;
        c.gridy = 3;
        btnGoBack.setFocusable(false);
        btnGoBack.addActionListener(menuSwitcher);
        pnlHolderRegister.add(btnGoBack, c);

        c.gridwidth = 1;
        c.gridheight = 1;
        c.gridx = 2;
        c.gridy = 3;
        btnSignUp.setFocusable(false);
        btnSignUp.addActionListener(menuSwitcher);
        btnSignUp.addActionListener(new ActionListener() {


            @Override
            public void actionPerformed(ActionEvent e) {

                Account signUp = new Account();
                String string1 = txtUsernameRegister.getText();
                String string2 = String.valueOf(txtPasswordRegister.getPassword());
                System.out.println(string2);
                signUp.setUserName(string1);
                signUp.setPassword(string2);

                if (string1.equals("") || string2.equals("")) {
                    lblErrorMessageRegister.setText("Vul alle velden in");
                    pnlErrorMessageRegister.setVisible(true);

                } else if (!checkAccount(signUp)) {
                    cardLayout.show(pnlHolder, "panelLogin");
                    //accounts.add(signUp);
                    accounts1.add(signUp);
                } else if (checkAccount(signUp)) {
                    lblErrorMessageRegister.setText("Account bestaat al");
                    pnlErrorMessageRegister.setVisible(true);
                }
                txtUsernameRegister.setText("");
                txtPasswordRegister.setText("");
            }
        });
        pnlHolderRegister.add(btnSignUp, c);

        //setPanelHome();
        revalidate();
    }

    public void setPanelHome() {
        pnlHome.setBackground(new Color(245, 250, 255));
        pnlHome.setLayout(borderLayout);

        pnlWest.setPreferredSize(new Dimension(150, 0));
        pnlWest.setBorder(raisedetched);
        pnlWest.setBackground(new Color(245, 255, 250));
        pnlWest.setLayout(gridBagLayout);
        pnlHome.add(pnlWest, BorderLayout.WEST);

        c.gridx = 0;
        c.gridy = 0;
        btnToSearchFunctionFromHome.addActionListener(menuSwitcher);
        pnlWest.add(btnToSearchFunctionFromHome, c);

        c.gridx = 0;
        c.gridy = 1;
        pnlWest.add(btnToNewFunction, c);

        //setPanelSearchFunction();
        revalidate();
    }

    public void setPanelSearchFunction() {
        pnlSearchFunction.removeAll();
        revalidate();
        pnlSearchFunction.setLayout(new GridLayout());

        //busTrips = new ArrayList<>();
//       ArrayList<TrainTrip> trainTrips = new ArrayList<>();

        // panel left
        pnlL = new JPanel();
        pnlL.setBackground(Color.orange);
        pnlL.setLayout(null);
        pnlSearchFunction.add(pnlL);

        //panel right
        pnlR = new JPanel();
        pnlR.setBackground(Color.green);
        pnlR.setLayout(null);
        pnlRoute.setBackground(Color.lightGray);
        pnlRoute.setBounds(10, 10, 315, 150);

        pnlStationInfo.setBackground(Color.lightGray);
        pnlStationInfo.setBounds(10, 170, 315, 150);

        pnlMap.setBackground(Color.lightGray);
        pnlMap.setBounds(10,350, 315,250);


        tripInfo_Label.setBounds(0, 0, 330, 300);
        pnlR.add(departure_Label);
        pnlR.add(arrival_Label);
        pnlR.add(departureTime_Label);
        pnlR.add(arrivalTime_Label);
        pnlRoute.add(tripInfo_Label);
        pnlR.add(pnlRoute);
        pnlR.add(pnlStationInfo);




        pnlSearchFunction.add(pnlR);
        Kaart kaart = new Kaart();
        //Splitpane
        JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, new JScrollPane(pnlL), new JScrollPane(pnlR));
        splitPane.setResizeWeight(0.5);
        pnlSearchFunction.add(splitPane);
        //image = new JLabel(); // image jl  abel
        btnMap.addActionListener(e ->{

        });
        pnlMap.add(btnMap);
        pnlR.add(pnlMap);
      /*  */

        rbtnBus.setBackground(Color.orange);
        rbtnBus.addActionListener(e -> {});
        rbtnBus.setBounds(120, 45, 60, 25);
        pnlL.add(rbtnBus);


        rbtnTrain = new JRadioButton("Train");
        rbtnTrain.setBackground(Color.orange);
        rbtnTrain.addActionListener(e -> {});
        rbtnTrain.setBounds(190, 45, 60, 25);
        pnlL.add(rbtnTrain);


        ButtonGroup group = new ButtonGroup();
        group.add(rbtnBus);
        group.add(rbtnTrain);

        defaultComboBoxModel1 = new DefaultComboBoxModel();
        defaultComboBoxModel1.insertElementAt("NO SELECTION", 0);

        addWithoutDuplicates(trips);

        for (TrainTrip trip : trainTrips) {
            defaultComboBoxModel1.addElement(trip.getName());
        }
        defaultComboBoxModel1.setSelectedItem("NO SELECTION");

        availableRoutes = new JComboBox(defaultComboBoxModel1);
        availableRoutes.setBounds(10, 10, 200, 25);
        pnlL.add(availableRoutes);

        defaultComboBoxModel2 = new DefaultComboBoxModel();
        defaultComboBoxModel2.insertElementAt("NO SELECTION", 0);
        for (String time : times) {
            defaultComboBoxModel2.addElement(time);
        }
        defaultComboBoxModel2.setSelectedItem("NO SELECTION");

        availableTimes = new JComboBox(defaultComboBoxModel2);
        availableTimes.setBounds(220, 10, 115, 25);
        pnlL.add(availableTimes);

       // btnSearchForTrips = new JButton("Zoek");
        btnSearchForTrips.setBounds(10, 45, 100, 25);
        btnSearchForTrips.addActionListener(e ->
        {
            if (e.getSource() == btnSearchForTrips) {
                System.out.println(trips.size());
                listModel.clear();
                if (availableRoutes.getSelectedItem().toString().equals("NO SELECTION") && availableTimes.getSelectedItem().toString().equals("NO SELECTION")) {
//                    for (TrainTrip t : trips)
//                    {
//                        listModel.addElement(t);
//                    }
                    String element;
                    if(languageValue == 0){
                         element = "Please make sure to select a route and a time";
                    }else{
                         element = "Selecteer alstublieft een route en een tijd";
                    }
                    listModel.addElement(element);

                }

                if (!availableRoutes.getSelectedItem().toString().equals("NO SELECTION") && availableTimes.getSelectedItem().toString().equals("NO SELECTION")) {
//                    listModel.clear();
                    //trips.removeIf(TrainTrip -> !TrainTrip.getName().equals(availableRoutes.getSelectedItem().toString()));
                    for (TrainTrip t : trips) {
                        if (compareRoute(t.getName(), availableRoutes.getSelectedItem().toString())) {
                            listModel.addElement(t);
                        }
                    }
                }

                if (availableRoutes.getSelectedItem().toString().equals("NO SELECTION") && !availableTimes.getSelectedItem().toString().equals("NO SELECTION")) {
                    // trips.removeIf(TrainTrip -> stringTimeToInt(TrainTrip.getD_time()) < stringTimeToInt(availableTimes.getSelectedItem().toString()));
//                    for (TrainTrip t : trips)
//                    {
//                        listModel.addElement(t);
//                    }
                    String x;
                    if(languageValue == 0){
                        x = "Please make sure to choose a route";
                    }else{
                        x = "Selecteer alstublieft een route";
                    }
                    listModel.addElement(x);
                }

                if (!availableRoutes.getSelectedItem().toString().equals("NO SELECTION") && !availableTimes.getSelectedItem().toString().equals("NO SELECTION")) {
                    // trips.removeIf(TrainTrip -> !TrainTrip.getName().equals(availableRoutes.getSelectedItem().toString()));
                    //trips.removeIf(TrainTrip -> stringTimeToInt(TrainTrip.getD_time()) < stringTimeToInt(availableTimes.getSelectedItem().toString()));

                    for (TrainTrip t : trips) {
                        if (compareRoute(t.getName(), availableRoutes.getSelectedItem().toString()) && checkTime(availableTimes.getSelectedItem().toString(), t.getD_time())) {
                            listModel.addElement(t);
                        }
                    }
                }
            }
            if (rbtnBus.isSelected()) {

                System.out.println(busTrips.size());
                listModel.clear();

                if (availableRoutes.getSelectedItem().toString().equals("NO SELECTION") && availableTimes.getSelectedItem().toString().equals("NO SELECTION")) {
                    String element = "Please make sure to select a route and a time";
                    listModel.addElement(element);
                }

                if (!availableRoutes.getSelectedItem().toString().equals("NO SELECTION") && availableTimes.getSelectedItem().toString().equals("NO SELECTION")) {
                    for (BusTrip t : busTrips) {
                        if (compareRoute(t.getName(), availableRoutes.getSelectedItem().toString())) {
                            listModel.addElement(t);
                        }
                    }
                }

                if (availableRoutes.getSelectedItem().toString().equals("NO SELECTION") && !availableTimes.getSelectedItem().toString().equals("NO SELECTION")) {
                    String x = "Please make sure to choose a route";
                    listModel.addElement(x);
                }
                if (!availableRoutes.getSelectedItem().toString().equals("NO SELECTION") && !availableTimes.getSelectedItem().toString().equals("NO SELECTION")) {
                    for (BusTrip t : busTrips) {
                        if (compareRoute(t.getName(), availableRoutes.getSelectedItem().toString()) && checkTime(availableTimes.getSelectedItem().toString(), t.getD_time())) {
                            listModel.addElement(t);
                        }
                    }
                }
            }
            if (!availableRoutes.getSelectedItem().toString().equals("NO SELECTION") && !availableTimes.getSelectedItem().toString().equals("NO SELECTION") && !rbtnBus.isSelected() && !rbtnTrain.isSelected()) {
                listModel.clear();
                String element = "Please make sure to choose a vehicle ";
                listModel.addElement(element);
            }
            if (!availableRoutes.getSelectedItem().toString().equals("NO SELECTION") && availableTimes.getSelectedItem().toString().equals("NO SELECTION") && !rbtnBus.isSelected() && !rbtnTrain.isSelected()) {
                listModel.clear();
                String element = "Please make sure to choose a time and a vehicle";
                listModel.addElement(element);
            }
            if (availableRoutes.getSelectedItem().toString().equals("NO SELECTION") && !availableTimes.getSelectedItem().toString().equals("NO SELECTION") && !rbtnBus.isSelected() && !rbtnTrain.isSelected()) {
                listModel.clear();
                String element = "Please make sure to choose a route and a vehicle";
                listModel.addElement(element);
            }
            if (!availableRoutes.getSelectedItem().toString().equals("NO SELECTION") && !availableTimes.getSelectedItem().toString().equals("NO SELECTION") && !rbtnBus.isSelected() && !rbtnTrain.isSelected())
//                    {

//                    }
                if (availableRoutes.getSelectedItem().toString().equals("NO SELECTION") && availableTimes.getSelectedItem().toString().equals("NO SELECTION") && !rbtnBus.isSelected() && !rbtnTrain.isSelected()) {
                    listModel.clear();
                    String element = "Please make sure to choose a route and a vehicle";
                    listModel.addElement(element);
                }

        });
        pnlL.add(btnSearchForTrips);

        listModel = new DefaultListModel();

        jList = new JList(listModel);
        jList.setVisibleRowCount(5);
        jList.addListSelectionListener(e ->

        {
            try {

                String y = jList.getSelectedValue().toString();


                String[] splitRoute = y.toString().split(": ");
                String name = splitRoute[0];

                String[] splitName = name.split(" - ");
                String dName = splitName[0];
                String aName = splitName[1];
                String times = splitRoute[1];

                String[] splitTimes = times.split(" - ");
                String dTime = splitTimes[0];
                String aTime = splitTimes[1];

                btnMap.addActionListener(l -> kaart.kaartData(dName, aName));


                dInfo.setText(get_dName(y) + " : " + compareStation(get_dName(y)) + "\n");
                pnlStationInfo.add(dInfo);
                aInfo.setText(get_aName(y) + " : " + compareStation(get_aName(y)));
                pnlStationInfo.add(aInfo);

                departure_Label.setText(dName);
                arrival_Label.setText(aName);

                departureTime_Label.setText(dTime);
                arrivalTime_Label.setText(aTime);

                departure_Label.setBounds(0, 330, 330, 300);
                arrival_Label.setBounds(100, 330, 330, 300);

                departureTime_Label.setBounds(0, 350, 330, 300);
                arrivalTime_Label.setBounds(50, 350, 330, 300);
                pnlRoute.add(departure_Label);
                pnlRoute.add(arrival_Label);
                pnlRoute.add(departureTime_Label);
                pnlRoute.add(arrivalTime_Label);
                pnlR.add(pnlRoute);

                pnlR.add(pnlMap);

                revalidate();
                if(loggedInAccount != null){
                    loggedInAccount.addToHistory(y);
                }

            } catch (NullPointerException x) {
                x.fillInStackTrace();
            }
        });

        revalidate();


        // Historie knop en favoriete route knop

        DefaultListModel listModel2 = new DefaultListModel();
        JList jList2 = new JList(listModel2);
        jList2.setVisibleRowCount(5);
        JScrollPane jScrollPaneForJList2 = new JScrollPane(jList2);
        jScrollPaneForJList2.setBounds(10, 400, 330, 250);
        pnlL.add(jScrollPaneForJList2);


        btnHistoryList.setBounds(210, 360, 100, 25);
        btnHistoryList.addActionListener(e ->

        {
            if (e.getSource() == btnHistoryList) {

                listModel2.clear();
                if(loggedInAccount != null){

                    for (Object o : loggedInAccount.getTravelHistory()) {

                        listModel2.addElement(o);
                    }



                    }
                else {
                    String notLoggedIn = "You are not logged in. This function can only work if you are logged in.";
                    listModel2.addElement(notLoggedIn);
                    revalidate();
                }
                    jList2.addListSelectionListener(h ->

                    {

                        pnlRoute.removeAll();

                        revalidate();
                        pnlRoute.add(tripInfo_Label);

                        Object y = jList2.getSelectedValue();

                        String[] splitRoute = y.toString().split(": ");

                        String name = splitRoute[0];
                        String[] splitName = name.split(" - ");
                        String dName = splitName[0];
                        String aName = splitName[1];

                        String times = splitRoute[1];
                        String[] splitTimes = times.split(" - ");
                        String dTime = splitTimes[0];
                        String aTime = splitTimes[1];

                        //btnMap.addActionListener(l -> kaart.kaartData(jList2));

                        departure_Label.setText(dName);
                        arrival_Label.setText(aName);

                        departureTime_Label.setText(dTime);
                        arrivalTime_Label.setText(aTime);

                        departure_Label.setBounds(0, 330, 330, 300);
                        arrival_Label.setBounds(100, 330, 330, 300);

                        departureTime_Label.setBounds(0, 350, 330, 300);
                        arrivalTime_Label.setBounds(50, 350, 330, 300);
                        pnlRoute.add(departure_Label);
                        pnlRoute.add(arrival_Label);
                        pnlRoute.add(departureTime_Label);
                        pnlRoute.add(arrivalTime_Label);
                        pnlR.add(pnlRoute);

                        revalidate();

                    });

                }



            });




        //btnSavedRoutes = new JButton("Favorieten");
        btnSavedRoutes.setBounds(10, 360, 100, 25);
        btnSavedRoutes.addActionListener(e ->

        {
            if (e.getSource() == btnSavedRoutes) {
                listModel2.clear();
                if(loggedInAccount != null){
                    for (Object obj : loggedInAccount.getSavedRoutes()) {
                        listModel2.addElement(obj);
                    }
                }else{
                    String notLoggedIn = "You are not logged in. This function can only work if you are logged in.";
                    listModel2.addElement(notLoggedIn);
                    revalidate();
                }

            }

        });


        btnSaveRoute.setBounds(110, 360, 100, 25);
        btnSaveRoute.addActionListener(e ->

        {
            if (e.getSource() == btnSaveRoute) {
                Object z = jList.getSelectedValue();
                if(loggedInAccount != null){
                    loggedInAccount.addSavedRoute(z);
                }else{
                    String notLoggedIn = "You are not logged in. This function can only work if you are logged in.";
                    listModel2.addElement(notLoggedIn);
                    revalidate();
                }
            }
        });

        pnlL.add(btnHistoryList);
        pnlL.add(btnSavedRoutes);
        pnlL.add(btnSaveRoute);
        jScrollPane = new

                JScrollPane(jList);
        jScrollPane.setBounds(10, 100, 330, 250);
        pnlL.add(jScrollPane);

        revalidate();

    }
    // Class voor switchen tussen verschillende menu's
    public class menuSwitcher implements ActionListener
    {
        public void actionPerformed(ActionEvent event)
        {
            JButton src = (JButton) event.getSource();
            pnlErrorMessageLogin.setVisible(false);

            if (src.equals(btnContinueWithoutAccount))
            {
                //setPanelSearchFunction();
                cardLayout.show(pnlHolder, "panelSearchScreen");
                btnTerugNaarHomeVanuitSearchFunction.setVisible(false);
            }
            if (src.equals(btnToSearchFunctionFromHome))
            {
                setPanelSearchFunction();
                cardLayout.show(pnlHolder, "panelSearchScreen");
                btnTerugNaarHomeVanuitSearchFunction.setVisible(true);
            }
            if (src.equals(btnLogout))
            {
                setPanelLogin();
                cardLayout.show(pnlHolder, "panelLogin");
            }
            if (src.equals(btnToRegister))
            {
                setPanelRegister();
                cardLayout.show(pnlHolder, "registratieScherm");
            }
            if (src.equals(btnGoBack))
            {
                setPanelLogin();
                cardLayout.show(pnlHolder, "panelLogin");
            }
            if (src.equals(btnTerugNaarHomeVanuitSearchFunction))
            {
                setPanelHome();
                cardLayout.show(pnlHolder, "panelHome");
            }
            txtUsernameRegister.setText("");
            txtPasswordRegister.setText("");
        }
    }

    public void addWithoutDuplicates(ArrayList<TrainTrip> trainTrips1)
    {
        boolean found = false;

        for(TrainTrip t : trainTrips1)
        {
            found = false;
            for(TrainTrip t2 : trainTrips)
            {
                if(t.getName().equals(t2.getName())){found = true;}
            }
            if(found==false)
            {
                trainTrips.add(t);
            }
        }

    }

    //Method that convert a time string to a integer (for example it converts 11:45 into 1145)
    public int stringTimeToInt(String time)
    {
        String newString =  time.replace(":","");
        int i = Integer.valueOf(newString);
        return i;
    }
    public void loadRoutesJSONTrain(File file , ArrayList<TrainTrip> trainTrips)
    {
        trainTrips.clear();

        try {
            InputStream inputStream = new FileInputStream(file);
            JSONTokener tokener = new JSONTokener(inputStream);
            while(tokener.more())
            {

                JSONObject object = new JSONObject(tokener);
                String name = object.getString("RouteName");
                String departureTime = object.getString("DepartureTime");
                String arrivalTime = object.getString("ArrivalTime");
                TrainTrip trainTrip= new TrainTrip(name,departureTime,arrivalTime);

                trainTrips.add(trainTrip);
                tokener.next();
            }


        } catch (FileNotFoundException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
        }
    }
    public void loadRoutesJSONBus(File file , ArrayList<BusTrip> busTrips)
    {
        trainTrips.clear();

        try {
            InputStream inputStream = new FileInputStream(file);
            JSONTokener tokener = new JSONTokener(inputStream);
            while(tokener.more())
            {

                JSONObject object = new JSONObject(tokener);
                String name = object.getString("RouteName");
                String departureTime = object.getString("DepartureTime");
                String arrivalTime = object.getString("ArrivalTime");
                BusTrip busTrip= new BusTrip(name,departureTime,arrivalTime);

                busTrips.add(busTrip);
                tokener.next();
            }


        } catch (FileNotFoundException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
        }
    }


    public boolean compareRoute(String string1 , String string2)
    {
        boolean checker = false;
        if(string1.equals(string2))
        {
            checker=true;
        }
        return checker;
    }

    public boolean checkTime(String selectedTime , String routeTime)
    {
        boolean checker1 = false;
        if(stringTimeToInt(routeTime)>=stringTimeToInt(selectedTime))
        {
            checker1=true;
        }
        return checker1;
    }

    // Controleren of account al bestaat (return true or false)
    public boolean checkAccount(Account account) {
        boolean status = false;
        for (Account a : accounts1.getAccounts()) {
            if (a.getUserName().equals(account.getUserName())) {
                status = true;
            }
        }
        return status;
    }

    //method to change the loggedIn situation for 1 account to true so i can use it later to keep it logged in.
    public void setTrue (Account account)
    {
        for (Account account1 : accounts1.getAccounts())
        {
            account1.setLoggedIn(account1.getUserName().equals(account.getUserName()));
        }
    }

    //method to make all accounts false in case remember me wasn't selected
    public void setFalse()
    {
        for(Account acco : accounts1.getAccounts())
        {
            acco.setLoggedIn(false);
        }
    }
    //method to check if any account was remembered
    public boolean checkIfAccountWasRemembered()
    {
        boolean loggedIn = false;
        for(Account account : accounts1.getAccounts())
        {
            if(account.getLoggedIn())
            {
                loggedIn = true;
                break;
            }

        }
        return loggedIn;
    }

    //Method to get last logged in account
    public void getLastLoggedInAccount()
    {
        for (Account account : accounts1.getAccounts())
        {
            if (account.getLoggedIn())
            {
                lastLoggedInAccount = account;
            }
        }
    }
    public void startProgram()
    {

        if(checkIfAccountWasRemembered())
        {
            getLastLoggedInAccount();
            loggedInAccount = lastLoggedInAccount;
            setPanelHome();
            cardLayout.show(pnlHolder,"panelHome");

        }
        else
        {
            setPanelLogin();
            cardLayout.show(pnlHolder,"panelLogin");
        }

    }

    public boolean checkAccount1(String s )
    {
        boolean status = false;
        for(Account account : accounts1.getAccounts())
        {
            if (account.getUserName().equals(s))
            {
                status = true;
                break;
            }

        }
        return status;
    }
    public String get_dName(String y) {

        String[] splitRoute = y.toString().split(": ");
        String name = splitRoute[0];

        String[] splitName = name.split(" - ");
        String dName = splitName[0];
        return dName;
    }
    public String get_dTime(String y) {

        String[] splitRoute = y.toString().split(": ");
        String time = splitRoute[1];

        String[] splitTime = time.split(" - ");

        String dTime = splitTime[0];
        return dTime;
    }
    public String get_aName(String y) {

        String[] splitRoute = y.toString().split(": ");
        String name = splitRoute[0];

        String[] splitName = name.split(" - ");
        String aName = splitName[1];

        return aName;
    }
    public String get_aTime(String y) {

        String[] splitRoute = y.toString().split(": ");
        String time = splitRoute[1];

        String[] splitTime = time.split(" - ");
        String result = splitTime[1];
        return result;
    }


    public String compareStation(String station) {
        //ArrayList<String> infoList = new ArrayList<>();
        ArrayList<Station> stationList = Station.getAllStationsFromJSON();
        String result = "null";
        for (Station inf : stationList) {
            if (inf.getName().equals(station)) {
                result = inf.getStationInfo();
            } else {
                System.out.println(station + " " + inf.getName() + " " + inf.getStationInfo());
            }
        }
        return result;
    }

}



