package com.test;

import org.json.simple.JSONObject;

public class Trip extends Route
{
    String d_time;
    String a_time;
    String filepath;

    Trip(String name, String d_time, String a_time)
    {
        super(name);
        this.d_time = d_time;
        this.a_time = a_time;
    }

    public String get_filepath()
    {
        return filepath;
    }
    public String getD_time()
    {
        return d_time;
    }

    public JSONObject toJson()
    {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("RouteName",name);
        jsonObject.put("DepartureTime",d_time);
        jsonObject.put("ArrivalTime",a_time);

        return jsonObject;
    }


    @Override
    public String toString()
    {
        return name + ": " + d_time + " - " + a_time;
    }
}
