package com.test;

import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.*;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class Route
{
    String name;

    public Route(String name)
    {
        this.name = name;
    }
    public Route(){}


    public void save(File file , ArrayList<TrainTrip> arrayList)
    {
        try {
            FileWriter fileWriter = new FileWriter(file);

            for(TrainTrip trip : arrayList)
            {
                if(arrayList.indexOf(trip)==arrayList.size()-1)
                {
                    fileWriter.write(trip.toJson().toString() + "," );
                }
                else
                {
                    fileWriter.write(trip.toJson().toString() + "," + "\n");
                }
            }

            fileWriter.close();

        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }



    public String getName()
    {
        return name;
    }

    @Override
    public String toString()
    {
        return name;
    }
}
