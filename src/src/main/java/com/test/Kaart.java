package com.test;

import javax.swing.*;

public class Kaart extends JFrame {

        JLabel image = new JLabel();
        String filepath;

    public void kaartData(String dName, String aName) { // Laadt kaart

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        setBounds(1200, 400, 1200, 900);
        setVisible(false);
        add(image);

        filepath = "src/src/images/" + dName + aName + ".png"; // File path samenstellen met departure en arrival
        image.setIcon(new ImageIcon(filepath)); // jLabel afbeelding toewijzen
        setVisible(true);

    }

}
